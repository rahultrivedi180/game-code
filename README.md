## Setting up server in Local & Getting started

Prerequisites:

1. Install nodejs from [here](https://nodejs.org/en/download)
2. Install mongodb compass from [here](https://www.mongodb.com/products/compass) to interact with database UI
3. Clone this repository
4. run command `npm install` in project's root directory
5. then run `npm run dev` to start the project in dev mode which enables hot reloading when making changes. or run `npm run build` to build the project and then run the project using command `npm run start`

**NOTE: You can also use deployed server to use it to test api and websocket**

## REST API Endpoints

Possible errors from API response are as follows:

-   `400 Validation Error` - The request is malformed or missing required parameters.

```json
{
    "error": "error message"
}
```

-   `403 Forbidden` - The request was valid, but the server is refusing action. This can be due to a database validation error.

```json
{
    "error": "Database validation error"
}
```

-   404 Not Found - The requested resource could not be found.

```json
{
    "error": "error message"
}
```

-   `500 Internal Server Error` - There was an internal server error during the request processing.

```json
{
    "error": "Internal server error"
}
```

### Endpoint: `/user/signup`

HTTP Method: **`POST`**
Request Body: JSON Object

```json
{
    "email": "string",
    "password": "string",
    "username": "string"
}
```

Parameters:

-   `email` (string, required) - The email of the user to sign up with.
-   `password` (string, required) - The password of the user to sign up with.
-   `username` (string, required) - The username of the user to sign up with.

If the request is successful, the API will return a `201 Created` status code along with a JSON object containing a success message and the newly created user ID:

```json
{
    "msg": "Account has been created",
    "data": {
        "userId": "1234567890"
    }
}
```

### Endpoint: `/user/signin`

HTTP Method: **`POST`**

Request Body: JSON Object

```json
{
    "email": "string",
    "password": "string"
}
```

Parameters:

-   email (string, required) - The email of the user to sign in with.
-   password (string, required) - The password of the user to sign in with.

Response:

If the sign-in attempt is successful, the API will return a 200 OK status code along with a JSON object containing a success message and the signed JWT token:

{
"msg": "Signed in",
"data": "signed jwt token"
}

The signed JWT token can be used to authenticate future requests to protected API endpoints.

### Endpoint: `/friend/list`

HTTP Method: **`GET`**

Headers:

-   **`Authorization`** (string, required) - A jwt token obtained from the /user/signin endpoint.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing the friends list, pending friend requests, and blocked users for the authenticated user:

```json
{
    "friends": [],
    "pendingRequests": [
        {
            "_id": "64493b1ae2f17e6b9ea74295",
            "requester": {
                "_id": "64314f47a75942c3fda85bd1",
                "username": "..."
            },
            "recipient": "64324678294748cb17de2bf8",
            "createdAt": "2023-04-26T14:54:18.628Z",
            "updatedAt": "2023-04-26T14:54:18.628Z",
            "__v": 0
        }
    ],
    "blockedUsers": []
}
```

### Endpoint: `/friend/add`

HTTP Method: **`POST`**

Headers:

Authorization (string, required) - A jwt token obtained from the /user/signin endpoint.

Request Body: JSON Object

```json
{
    "requester": "requester id",
    "recipient": "recipient id"
}
```

Parameters:

-   `requester` (string, required) - The id of the user sending the friend request.
-   `recipient` (string, required) - The id of the user receiving the friend request.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing a success message:

```json
{
    "msg": "Friend Request Sent"
}
```

### Endpoint: `/friend/block`

HTTP Method: **`POST`**

Headers:

`Authorization` (string, required) - A jwt token obtained from the /user/signin endpoint.

Request Body: JSON Object

```json
{
    "requester": "requester id",
    "recipient": "recipient id"
}
```

Parameters:

-   `requester` (string, required) - The id of the user initiating the block.
-   `recipient` (string, required) - The id of the user being blocked.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing a boolean value indicating whether the block operation was successful:

```json
{
    "result": true
}
```

### Endpoint: `/friend/accept`

HTTP Method: `POST`

Headers:

`Authorization` (string, required) - A jwt token obtained from the /user/signin endpoint.

Request Body: JSON Object

```json
{
    "requester": "requester id",
    "recipient": "recipient id"
}
```

Parameters:

-   `requester` (string, required) - The id of the user who sent the friend request.
-   `recipient` (string, required) - The id of the user who received the friend request.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing a boolean value indicating whether the friend request was successfully accepted:

```json
{
    "result": true
}
```

### Endpoint: `/friend/reject`

HTTP Method: `POST`

Headers:

`Authorization` (string, required) - A jwt token obtained from the /user/signin endpoint.

Request Body: JSON Object

```json
{
    "requester": "requester id",
    "recipient": "recipient id"
}
```

Parameters:

-   `requester` (string, required) - The id of the user who is rejecting the friend request.
-   `recipient` (string, required) - The id of the user whose friend request is being rejected.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing a boolean value indicating whether the friend request was successfully rejected:

```json
{
    "result": true
}
```

### Endpoint: `/deck?userId=645..&characterId=...`

HTTP Method: `GET`

Headers:

`Authorization` (string, required) - A jwt token obtained from the /user/signin endpoint.

Query Parameters:

-   `userId` (string, required) - The id of the user who is rejecting the friend request.
-   `characterId` (string) - Character id of the deck. Pass characterId if you require only specific deck in the response.

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing deck or decks:

Response for decks:

```json
[
    {
        "characterId": "...",
        "deck": [...],
        "_id": "64621e655b76b960fdf239d0"
    },
    {
        "characterId": "...",
        "deck": [...],
        "_id": "64621e655b76b960fdf239d1"
    },
    {
        "characterId": "...",
        "deck": [...],
        "_id": "64621e655b76b960fdf239d2"
    },
    {
        "characterId": "...",
        "deck": [...],
        "_id": "64621e655b76b960fdf239d3"
    }
]
```

Response for deck will contain only specific object from the array of decks.

### Endpoint: `/deck`

HTTP Method: `PATCH`

Headers:

`Authorization` (string, required) - A jwt token obtained from the /user/signin endpoint.

Query Parameters:

-   `characterId` (string, required) - The id of the user who is rejecting the friend request.
-   `deck` (string) - A deck containing maximum of 28 hex keys of cards (ex: ["..."])

Response:

If the request is successful, the API will return a 200 OK status code along with a JSON object containing updated deck:

Response for decks:

```json
{
    "deck": {
        "characterId": "...",
        "deck": ["...", ...],
        "_id": "..."
    }
}
```
