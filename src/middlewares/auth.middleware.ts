import { JsonWebTokenError } from 'jsonwebtoken';

import jwtUtil from '../utilities/jwt.util';
import userValidators from '../validators/user.validators';

import type { NextFunction, Request, Response } from 'express';
import { returnError } from '../utilities/errors.util';

export default async function authMiddleware(
    req: Request,
    _res: Response,
    next: NextFunction
): Promise<any> {
    try {
        const token = req.headers.authorization;
        if (!token) {
            return next(returnError('UNAUTHORIZED_REQUEST'));
        }
        const decodedToken = jwtUtil.verify(token) as any;

        const invalidDecodedToken =
            userValidators.validateDecodedTokenObject(decodedToken);

        if (invalidDecodedToken) {
            return next(invalidDecodedToken);
        }

        req.user = {
            _id: decodedToken._id,
            email: decodedToken.email,
            username: decodedToken.username
        };

        next();
    } catch (error: any) {
        if (error instanceof JsonWebTokenError) {
            return next(returnError('TOKEN_VALIDATION_ERROR', error.message));
        }

        next(returnError(undefined, error.message ?? 'Something went wrong'));
    }
}
