import { any, enums, number, object, optional, string } from 'superstruct';

export const FFAOnJoinPayload = object({
    event: enums(['freeForAll/onJoin']),
    data: object({
        boardId: string(),
        characterId: optional(string())
    })
});

export const FFAOnGameStartPayload = object({
    event: enums(['freeForAll/onGameStart']),
    data: object({
        gameId: string()
    })
});

export const FFAOnGameUpdatePayload = object({
    event: enums(['freeForAll/onGameUpdate']),
    data: object({
        gameId: string(),
        cardKey: number(),
        actionKey: number(),
        updatedBoard: any(),
        currentPlayerId: string(),
        nextPlayerId: string()
    })
});

export const FFAOnLeavePayload = object({
    event: enums(['freeForAll/onLeave']),
    data: object({
        gameId: string()
    })
});

export const FFAOnGameOver = object({
    event: enums(['freeForAll/onGameOver']),
    data: object({
        gameId: string(),
        winnerId: string()
    })
});
