import { is, object, string, validate } from 'superstruct';
import { returnError } from '../utilities/errors.util';

const FriendRequestStatusUpdateBody = object({
    requester: string(),
    recipient: string()
});

const ListFriendsParams = object({
    recipient: string()
});

function validateFriendRequestStatusUpdateBody(body: unknown) {
    if (!is(body, FriendRequestStatusUpdateBody)) {
        const [error] = validate(body, FriendRequestStatusUpdateBody);
        if (!error) {
            return returnError('ADD_FRIEND_UNEXPECTED_VALIDATION_ERROR');
        }
        if (error.key === 'requester') {
            return returnError('INVALID_REQUESTER', error.message);
        }
        if (error.key === 'recipient') {
            return returnError('INVALID_RECIPIENT', error.message);
        }
    }
    return null;
}

export default {
    validateFriendRequestStatusUpdateBody,
    ListFriendsParams
};
