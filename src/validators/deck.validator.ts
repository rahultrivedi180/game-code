import { array, object, optional, string } from 'superstruct';

export interface IUpdateDeckRequestBody {
    operation: 'SHUFFLE_DECK';
    args: any;
}

const GetDeck = object({
    userId: string(),
    characterId: optional(string())
});

const UpdateDeck = object({
    characterId: string(),
    deck: array(string())
});

export default {
    GetDeck,
    UpdateDeck
};
