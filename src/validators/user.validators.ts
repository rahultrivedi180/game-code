import {
    array,
    is,
    number,
    object,
    optional,
    string,
    union,
    validate
} from 'superstruct';

import { Email, Password, Username } from './fields.validators';
import { returnError } from '../utilities/errors.util';

const SignupRequestBody = object({
    email: Email,
    password: Password,
    username: Username
});

const SigninRequestBody = object({
    email: Email,
    password: Password
});

const DecodedUser = object({
    _id: string(),
    email: Email,
    username: Username,
    iss: optional(string()),
    sub: optional(string()),
    aud: optional(union([string(), array(string())])),
    exp: optional(number()),
    nbf: optional(number()),
    iat: optional(number()),
    jti: optional(string())
});

function validateSignupRequestBody(body: unknown) {
    if (!is(body, SignupRequestBody)) {
        const [error] = validate(body, SignupRequestBody);
        if (!error) {
            return returnError('SIGNUP_UNEXPECTED_VALIDATION_ERROR');
        }
        if (error.key === 'username') {
            return returnError('INVALID_USERNAME', error.message);
        }
        if (error.key === 'email') {
            return returnError('INVALID_EMAIL', error.message);
        }
        if (error.key === 'password') {
            return returnError('INVALID_PASSWORD', error.message);
        }
    }
    return null;
}

function validateSigninRequestBody(body: unknown) {
    if (!is(body, SigninRequestBody)) {
        const [error] = validate(body, SignupRequestBody);
        if (!error) {
            return returnError('SIGNIN_UNEXPECTED_VALIDATION_ERROR');
        }
        if (error.key === 'email') {
            return returnError('INVALID_EMAIL', error.message);
        }
        if (error.key === 'password') {
            return returnError('INVALID_PASSWORD', error.message);
        }
    }
    return null;
}

function validateDecodedTokenObject(tokenObject: unknown) {
    if (!is(tokenObject, DecodedUser)) {
        const [error] = validate(tokenObject, DecodedUser);
        if (!error) {
            return returnError('TOKEN_UNEXPECTED_VALIDATION_ERROR');
        }

        return returnError('TOKEN_VALIDATION_ERROR', error.message);
    }

    return null;
}

export default {
    DecodedUser,
    validateSignupRequestBody,
    validateSigninRequestBody,
    validateDecodedTokenObject
};
