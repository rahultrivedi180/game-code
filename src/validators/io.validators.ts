// actionKey, cardKey, currentPlayerId, gameId, nextPlayerId, updatedBoard;

import { array, is, number, object, string, validate } from 'superstruct';
import { returnError } from '../utilities/errors.util';

const FFAListenOnGameUpdatePayload = object({
    gameId: string(),
    updatedBoard: array(array(string())),
    actionKey: number(),
    cardKey: number(),
    currentPlayerId: string(),
    nextPlayerId: string()
});

function validateFFAListenOnGameUpdatePayload(payload: unknown) {
    if (!is(payload, FFAListenOnGameUpdatePayload)) {
        const [error] = validate(payload, FFAListenOnGameUpdatePayload);
        if (!error) {
            return returnError('SIGNUP_UNEXPECTED_VALIDATION_ERROR');
        }

        if (error.key) {
            return returnError('INVALID_PAYLOAD', `invalid ${error.key}`);
        }
    }
    return null;
}

const FFAListenOnGameOverPayload = object({
    gameId: string(),
    winnerId: string()
});

function validateFFAListenOnGameOverPayload(payload: unknown) {
    if (!is(payload, FFAListenOnGameOverPayload)) {
        const [error] = validate(payload, FFAListenOnGameOverPayload);
        if (!error) {
            return returnError('SIGNUP_UNEXPECTED_VALIDATION_ERROR');
        }

        if (error.key) {
            return returnError('INVALID_PAYLOAD', `invalid ${error.key}`);
        }
    }
    return null;
}

export default {
    validateFFAListenOnGameUpdatePayload,
    validateFFAListenOnGameOverPayload
};
