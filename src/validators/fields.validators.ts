import { pattern, size, string } from 'superstruct';

import lengthLimitUtil from '../utilities/lengthLimit.util';
import regexUtil from '../utilities/regex.util';

export const Username = size(
    pattern(string(), regexUtil.usernameRegex),
    lengthLimitUtil.username.min,
    lengthLimitUtil.username.max
);

export const Email = size(
    pattern(string(), regexUtil.emailRegex),
    lengthLimitUtil.email.min,
    lengthLimitUtil.email.max
);

export const Password = string();

// export const Password = size(
//     pattern(string(), regexUtil.passwordRegex),
//     lengthLimitUtil.password.min,
//     lengthLimitUtil.password.max
// );
