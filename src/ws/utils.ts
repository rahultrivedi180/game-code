import { WebSocket } from 'ws';

export const userMap = new Map<string, WebSocket>();
export const gameMap = new Map<string, Map<string, WebSocket>>();

function createGameRoom(gameId: string) {
    const gameRoom = gameMap.get(gameId);

    if (gameRoom) {
        return gameRoom;
    }

    return gameMap.set(gameId, new Map()).get(gameId);
}

function addPlayerToGameRoom(
    gameId: string,
    playerId: string,
    playerSocket: WebSocket
) {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return false;
    }

    gameRoom.set(playerId, playerSocket);
    return true;
}

function getAllSocketsFromGameRoom(gameId: string) {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return null;
    }

    return [...gameRoom.values()];
}

function getSocketsExceptSelfFromGameRoom(gameId: string, self: string) {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return null;
    }

    const sockets: WebSocket[] = [];

    gameRoom.forEach((value, key) => {
        if (key !== self) {
            sockets.push(value);
        }
    });

    return sockets;
}

function getSelfFromGameRoom(gameId: string, self: string): WebSocket | null {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return null;
    }

    let selfSocket: WebSocket | null = null;

    gameRoom.forEach((value, key) => {
        if (key === self) {
            selfSocket = value;
        }
    });

    return selfSocket;
}

function deleteGameRoom(gameId: string) {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return true;
    }

    return gameRoom.delete(gameId);
}

function deletePlayerFromGame(gameId: string, playerId: string) {
    const gameRoom = gameMap.get(gameId);

    if (!gameRoom) {
        return true;
    }

    const player = gameRoom.get(playerId);

    if (!player) {
        return true;
    }

    return gameRoom.delete(playerId);
}

function addUserToUserMap(userId: string, socket: WebSocket) {
    const user = userMap.get(userId);

    if (!user) {
        userMap.set(userId, socket);
        return true;
    }

    return true;
}

function getUserFromUserMap(userId: string) {
    return userMap.get(userId);
}

function deleteUserFromUserMap(userId: string) {
    return userMap.delete(userId);
}

function sendDataToSocket(socket: WebSocket, data: any) {
    socket.send(JSON.stringify({ type: 'data', data }));
}

function sendErrorToSocket(socket: WebSocket, error: any) {
    socket.send(
        JSON.stringify({
            type: 'error',
            error
        })
    );
}

export default {
    createGameRoom,
    addPlayerToGameRoom,
    getAllSocketsFromGameRoom,
    getSocketsExceptSelfFromGameRoom,
    getSelfFromGameRoom,
    deleteGameRoom,
    deletePlayerFromGame,
    addUserToUserMap,
    getUserFromUserMap,
    deleteUserFromUserMap,
    sendDataToSocket,
    sendErrorToSocket
};
