import QueryString from 'qs';
import { is, string, validate } from 'superstruct';
import url from 'url';
import { IInfo, TCallback } from '../interfaces/ws';
import jwtUtil from '../utilities/jwt.util';
import userValidators from '../validators/user.validators';

function authMiddleware(info: IInfo, cb: TCallback) {
    try {
        const secWebsocketProtocol = info.req.headers['sec-websocket-protocol'];

        if (!info.req.url) {
            return cb(false, 401, 'Unauthorized');
        }

        const parsedUrl = url.parse(info.req.url);

        if (!parsedUrl.query) {
            return cb(false, 401, 'Unauthorized');
        }

        const parsedQueryObject = QueryString.parse(parsedUrl.query);

        if (!is(parsedQueryObject['token'], string())) {
            return cb(false, 401, 'Unauthorized');
        }

        const token =
            secWebsocketProtocol?.split(', ')[1] || parsedQueryObject['token'];

        if (!token) {
            return cb(false, 401, 'Unauthorized');
        }

        const decodedToken = jwtUtil.verify(token);

        if (!is(decodedToken, userValidators.DecodedUser)) {
            const [error] = validate(decodedToken, userValidators.DecodedUser);
            if (!error) {
                return cb(false, 401, 'Unauthorized');
            }

            return cb(false, 401, 'Unauthorized');
        }

        const { _id, email, username } = decodedToken;
        info.req.user = { _id, email, username };
        cb(true);
    } catch (error) {
        cb(false);
    }
}

export default {
    authMiddleware
};
