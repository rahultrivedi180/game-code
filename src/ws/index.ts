import WebSocket from 'ws';
import socketMiddleware from './socket.middleware';
import { IIncomingMessageWithUser } from '../interfaces/ws';
import onClose from './controllers/onClose';
import onMessage from './controllers/onMessage';
import onError from './controllers/onError';
import onConnection from './controllers/onConnection';

export const websocketServer = new WebSocket.Server({
    noServer: true,
    verifyClient: socketMiddleware.authMiddleware
});

websocketServer.on(
    'connection',
    (socket, request: IIncomingMessageWithUser) => {
        onConnection(socket, request); // Temporarily using onOpen on connection.
        socket.on('error', onError.bind(null, socket).bind(null, request));
        socket.on('message', onMessage.bind(null, socket).bind(null, request));
        socket.on('close', onClose.bind(null, socket).bind(null, request));
        socket.on('error', (err) => {
            console.log(`log2`, err);
        });
    }
);
