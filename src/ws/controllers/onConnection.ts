import { WebSocket } from 'ws';
import { IIncomingMessageWithUser } from '../../interfaces/ws';
import utils, { userMap } from '../utils';
import userModel from '../../models/user.model';
import { returnError } from '../../utilities/errors.util';

async function onConnection(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser
) {
    const user = _request.user;

    const userStatus = await userModel.updateUserStatus(user._id, 'online');

    if (!userStatus) {
        const { code, message, statusCode } = returnError(
            'UPDATE_ERROR',
            'Could not update user status'
        );

        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }

    userMap.forEach((socket) => {
        if (socket.readyState === WebSocket.OPEN) {
            utils.sendDataToSocket(socket, {
                event: 'connected',
                data: {
                    username: user.username,
                    userStatus
                }
            });
        }
    });

    utils.addUserToUserMap(user._id, _socket);

    utils.sendDataToSocket(_socket, {
        event: 'connected',
        data: {
            username: user.username,
            userStatus
        }
    });
}

export default onConnection;
