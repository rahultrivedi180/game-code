import { WebSocket } from 'ws';
import { IIncomingMessageWithUser } from '../../interfaces/ws';

function onError(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _err: Error
) {
    console.log(_err);
}

export default onError;
