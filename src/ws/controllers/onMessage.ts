import { WebSocket, RawData } from 'ws';
import wsDataParserUtil from '../../utilities/wsDataParser.util';
import { IIncomingMessageWithUser } from '../../interfaces/ws';
import { is } from 'superstruct';
import {
    FFAOnGameOver,
    FFAOnGameStartPayload,
    FFAOnGameUpdatePayload,
    FFAOnJoinPayload,
    FFAOnLeavePayload
} from '../../validators/wsPayload.validators';
import onJoin from './events/freeForAll/onJoin';
import onGameStart from './events/freeForAll/onGameStart';
import onGameUpdate from './events/freeForAll/onGameUpdate';
import onLeave from './events/freeForAll/onLeave';
import onGameOver from './events/freeForAll/onGameOver';

function onMessage(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _data: RawData,
    _isBinary: boolean
) {
    const parsedData = wsDataParserUtil.parseWsData(_data);

    if (is(parsedData, FFAOnJoinPayload)) {
        onJoin(_socket, _request, parsedData.data);
    }

    if (is(parsedData, FFAOnGameStartPayload)) {
        onGameStart(_socket, _request, parsedData.data);
    }

    if (is(parsedData, FFAOnGameUpdatePayload)) {
        onGameUpdate(_socket, _request, parsedData.data as any);
    }

    if (is(parsedData, FFAOnLeavePayload)) {
        onLeave(_socket, _request, parsedData.data);
    }

    if (is(parsedData, FFAOnGameOver)) {
        onGameOver(_socket, _request, parsedData.data);
    }
}

export default onMessage;
