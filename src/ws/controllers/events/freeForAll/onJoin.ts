import { WebSocket } from 'ws';
import {
    IIncomingMessageWithUser,
    TPayloadData
} from '../../../../interfaces/ws';
import {
    TFFAJoinedPayload,
    TFFAOnJoinPayload
} from '../../../../interfaces/ws/ffa.interface';
import userModel from '../../../../models/user.model';
import boardModel from '../../../../socket/freeForAll/models/board.model';
import GameModel from '../../../../socket/freeForAll/models/game.model';
import ffaUtils from '../../../../socket/freeForAll/utils';
import { returnError } from '../../../../utilities/errors.util';
import utils from '../../../utils';
import deckApi from '../../../../api/deck.api';

const gameModel = new GameModel();

async function sendSocketData(
    userId: string,
    gameId: string,
    newBoard: string[][],
    _socket: WebSocket,
    characterId?: string
) {
    let allCards = undefined;
    if (characterId) {
        const [_, shuffledDeck] = await Promise.all([
            userModel.updateUserStatus(userId, 'in-game'),
            deckApi.shuffleDeck(userId, gameId, characterId)
        ]);
        allCards = shuffledDeck?.allCards;
    } else {
        await userModel.updateUserStatus(userId, 'in-game');
    }
    utils.addPlayerToGameRoom(gameId, userId, _socket);

    const payload: TPayloadData<TFFAJoinedPayload> = {
        event: 'freeForAll/joined',
        data: {
            board: newBoard,
            gameId: gameId,
            playerId: userId
        }
    };

    const otherPlayers = utils.getSocketsExceptSelfFromGameRoom(gameId, userId);
    const self = utils.getSelfFromGameRoom(gameId, userId);
    otherPlayers?.forEach((socket) => {
        utils.sendDataToSocket(socket, payload);
    });
    if (self) {
        payload.data.deck = allCards ?? [];
        utils.sendDataToSocket(self, payload);
    }
}

async function onJoin(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _payload: TFFAOnJoinPayload
) {
    try {
        const user = _request.user;
        const { boardId, characterId } = _payload;

        const [userInDb, board] = await Promise.all([
            userModel.getUserById(user._id),
            boardModel.getBoardByBoardId(boardId)
        ]);

        if (userInDb?.userStatus === 'in-game') {
            throw returnError('PLAYER_INGAME');
        }

        if (userInDb?.userStatus === 'offline') {
            throw returnError('PLAYER_OFFLINE');
        }

        if (!board) {
            throw returnError('BOARD_NOT_FOUND');
        }

        const gamesToJoin = await gameModel.findGamesToJoin(board);
        let gameId = null;
        let newBoard = null;

        if (gamesToJoin.length < 1) {
            const newGame = await gameModel.intializeGame(board, [user._id]);
            gameId = newGame._id;
            newBoard = board.board;
            utils.createGameRoom(gameId.toString());
            await sendSocketData(
                user._id,
                gameId.toString(),
                newBoard,
                _socket,
                characterId
            );
            return;
        } else {
            const game = gamesToJoin[0];

            game.players.forEach((value) => {
                if (value.toString() === user._id) {
                    throw returnError('PLAYER_INGAME');
                }
            });

            gameId = game._id.toString();

            const position = board.totalPlayers - game.players.length;
            const layout = ffaUtils.layouts.find((layout) => {
                return layout.boardId === boardId;
            });

            if (!layout) {
                throw returnError('BOARD_NOT_FOUND');
            }

            if (game.players.length >= layout.totalPlayers) {
                // TODO: MAKE A SEPERATE FUCNTION
                const newGame = await gameModel.intializeGame(board, [
                    user._id
                ]);
                gameId = newGame._id;
                newBoard = board.board;
                utils.createGameRoom(gameId.toString());
                await sendSocketData(
                    user._id,
                    gameId.toString(),
                    newBoard,
                    _socket,
                    characterId
                );
                return;
            }

            const x = layout.playerPositions[position].x;
            const y = layout.playerPositions[position].y;

            newBoard = game.board;
            newBoard[x][y] = user._id.toString();

            await gameModel.addPlayerToGame(
                game._id.toString(),
                user._id.toString(),
                newBoard,
                { x, y }
            );
            await sendSocketData(
                user._id,
                gameId.toString(),
                newBoard,
                _socket,
                characterId
            );
        }
    } catch (error: any) {
        const { code, message, statusCode } = returnError(
            undefined,
            error.message ?? 'Something went wrong'
        );
        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }
}

export default onJoin;
