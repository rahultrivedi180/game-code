import { WebSocket } from 'ws';
import {
    IIncomingMessageWithUser,
    TPayloadData
} from '../../../../interfaces/ws';
import {
    TFFAGameStartedPayload,
    TFFAOnGameStartPayload
} from '../../../../interfaces/ws/ffa.interface';
import { returnError } from '../../../../utilities/errors.util';
import GameModel from '../../../../socket/freeForAll/models/game.model';
import utils from '../../../utils';
import userModel from '../../../../models/user.model';

const gameModel = new GameModel();

async function onGameStart(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _payload: TFFAOnGameStartPayload
) {
    try {
        const user = _request.user;
        const { gameId } = _payload;

        const [userInDb, game] = await Promise.all([
            userModel.getUserByParam('username', user.username),
            gameModel.getGameById(gameId)
        ]);

        if (!game) {
            const { code, message, statusCode } = returnError('GAME_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (!userInDb) {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (game.owner.toString() !== userInDb._id.toString()) {
            const { code, message, statusCode } = returnError(
                'PLAYER_NOT_GAME_OWNER'
            );
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (userInDb.userStatus !== 'in-game') {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_INGAME');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (game.gameStatus !== 'notStarted') {
            const { code, message, statusCode } =
                returnError('GAME_NOT_STARTED');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        await gameModel.startGame(gameId, user._id);

        const payload: TPayloadData<TFFAGameStartedPayload> = {
            event: 'freeForAll/gameStarted',
            data: {
                board: game.board,
                gameId
            }
        };

        const gameSockets = utils.getAllSocketsFromGameRoom(gameId.toString());
        gameSockets?.forEach((socket) => {
            utils.sendDataToSocket(socket, payload);
        });
    } catch (error: any) {
        const { code, message, statusCode } = returnError(
            undefined,
            error.message ?? 'Something went wrong'
        );
        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }
}

export default onGameStart;
