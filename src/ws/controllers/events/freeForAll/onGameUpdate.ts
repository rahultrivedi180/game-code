import { WebSocket } from 'ws';
import {
    IIncomingMessageWithUser,
    TPayloadData
} from '../../../../interfaces/ws';
import {
    TFFAGameUpdatedPayload,
    TFFAOnGameUpdatePayload,
    TFFATurnUpdate
} from '../../../../interfaces/ws/ffa.interface';
import { returnError } from '../../../../utilities/errors.util';
import utils from '../../../utils';
import GameModel from '../../../../socket/freeForAll/models/game.model';
import userModel from '../../../../models/user.model';
import deckApi from '../../../../api/deck.api';

const gameModel = new GameModel();

async function onGameUpdate(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _payload: TFFAOnGameUpdatePayload
) {
    try {
        const user = _request.user;
        const {
            actionKey,
            cardKey,
            currentPlayerId,
            gameId,
            nextPlayerId,
            updatedBoard
        } = _payload;

        const [userInDb, game] = await Promise.all([
            userModel.getUserById(user._id),
            gameModel.model.findById(gameId).lean()
        ]);

        if (!game) {
            const { code, message, statusCode } = returnError('GAME_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (!userInDb) {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (game.gameStatus !== 'started') {
            const { code, message, statusCode } = returnError(
                'INVALID_GAME_STATUS',
                `game has been ${game.gameStatus}`
            );
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (userInDb.userStatus !== 'in-game') {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_INGAME');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        await gameModel.model.findOneAndUpdate(
            { _id: _payload.gameId },
            {
                $set: { board: updatedBoard, turn: nextPlayerId },
                $push: {
                    moves: { playerId: currentPlayerId, cardKey, actionKey }
                }
            }
        );

        const payload: TPayloadData<TFFAGameUpdatedPayload> = {
            event: 'freeForAll/gameUpdated',
            data: {
                actionKey,
                cardKey,
                currentPlayerId,
                gameId,
                nextPlayerId,
                updatedBoard
            }
        };

        const gameSockets = utils.getAllSocketsFromGameRoom(gameId.toString());
        gameSockets?.forEach((socket) => {
            utils.sendDataToSocket(socket, payload);
        });

        const turnPayload: TPayloadData<TFFATurnUpdate> = {
            event: 'freeForAll/turnSet',
            data: {
                gameId,
                turn: nextPlayerId
            }
        };

        if (_payload.drawCards) {
            await deckApi.drawCards(user._id, gameId);
        }

        gameSockets?.forEach((socket) => {
            utils.sendDataToSocket(socket, turnPayload);
        });
    } catch (error: any) {
        const { code, message, statusCode } = returnError(
            undefined,
            error.message ?? 'Something went wrong'
        );
        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }
}

export default onGameUpdate;
