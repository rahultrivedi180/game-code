import { WebSocket } from 'ws';
import {
    IIncomingMessageWithUser,
    TPayloadData
} from '../../../../interfaces/ws';
import {
    TFFAGameDestroyed,
    TFFAOnLeavePayload,
    TFFAPlayerLeftPayload,
    TFFAWinnerAnnounced
} from '../../../../interfaces/ws/ffa.interface';
import { returnError } from '../../../../utilities/errors.util';
import gameUtils from '../../../../socket/freeForAll/utils';
import utils from '../../../utils';
import GameModel from '../../../../socket/freeForAll/models/game.model';
import userModel from '../../../../models/user.model';

const gameModel = new GameModel();

async function onLeave(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _payload: TFFAOnLeavePayload
) {
    try {
        const user = _request.user;
        const { gameId } = _payload;

        const [userInDb, game] = await Promise.all([
            userModel.getUserById(user._id),
            gameModel.getGameById(gameId)
        ]);

        if (!game) {
            const { code, message, statusCode } = returnError('GAME_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (!userInDb) {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        const { owner, winnerAnnounced, gameDestroyed } = gameUtils.playerLeave(
            game,
            userInDb
        );

        const update: Record<string, any> = {
            $pull: {
                players: userInDb._id,
                currentPositions: { playerId: userInDb._id }
            }
        };

        if (owner) {
            update['owner'] = owner;
        }

        if (winnerAnnounced) {
            update['winner'] = winnerAnnounced;
            update['gameStatus'] = 'finished';
        }

        if (gameDestroyed) {
            update['gameStatus'] = 'cancelled';
        }

        await Promise.all([
            gameModel.model.updateOne({ _id: gameId }, update),
            userModel.updateUserStatus(userInDb._id.toString(), 'online')
        ]);

        const payload: TPayloadData<TFFAPlayerLeftPayload> = {
            event: 'freeForAll/playerLeft',
            data: {
                gameId,
                playerId: user._id
            }
        };

        const gameSockets = utils.getAllSocketsFromGameRoom(gameId.toString());
        gameSockets?.forEach((socket) => {
            utils.sendDataToSocket(socket, payload);
        });

        utils.deletePlayerFromGame(gameId, user._id);

        if (winnerAnnounced) {
            payload.data.winner = winnerAnnounced;

            const winnerPayload: TPayloadData<TFFAWinnerAnnounced> = {
                event: 'freeForAllWinnerAnnounced',
                data: {
                    gameId,
                    winner: winnerAnnounced
                }
            };

            gameSockets?.forEach((socket) => {
                utils.sendDataToSocket(socket, winnerPayload);
            });

            utils.deleteGameRoom(gameId);
        }

        if (gameDestroyed) {
            const gameDestroyedPayload: TPayloadData<TFFAGameDestroyed> = {
                event: 'freeForAll/gameDestroyed',
                data: { gameId }
            };
            gameSockets?.forEach((socket) => {
                utils.sendDataToSocket(socket, gameDestroyedPayload);
            });
        }
    } catch (error: any) {
        const { code, message, statusCode } = returnError(
            undefined,
            error.message ?? 'Something went wrong'
        );
        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }
}

export default onLeave;
