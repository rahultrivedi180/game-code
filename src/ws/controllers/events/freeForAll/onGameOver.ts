import { WebSocket } from 'ws';
import {
    IIncomingMessageWithUser,
    TPayloadData
} from '../../../../interfaces/ws';
import {
    TFFAGameOverPayload,
    TFFAOnGameOverPayload
} from '../../../../interfaces/ws/ffa.interface';
import { returnError } from '../../../../utilities/errors.util';
import utils from '../../../utils';
import GameModel from '../../../../socket/freeForAll/models/game.model';
import userModel from '../../../../models/user.model';

const gameModel = new GameModel();

async function onGameOver(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _payload: TFFAOnGameOverPayload
) {
    try {
        const user = _request.user;
        const { gameId } = _payload;

        const [userInDb, game] = await Promise.all([
            userModel.getUserById(user._id),
            gameModel.model.findById(gameId).lean()
        ]);

        if (!game) {
            const { code, message, statusCode } = returnError('GAME_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (!userInDb) {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_FOUND');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (game.gameStatus !== 'started') {
            const { code, message, statusCode } = returnError(
                'INVALID_GAME_STATUS',
                `game has been ${game.gameStatus}`
            );
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        if (userInDb.userStatus !== 'in-game') {
            const { code, message, statusCode } =
                returnError('PLAYER_NOT_INGAME');
            utils.sendErrorToSocket(_socket, { code, message, statusCode });
            return;
        }

        await gameModel.model.findOneAndUpdate(
            { _id: _payload.gameId },
            { $set: { winner: user._id, gameStatus: 'finished' } }
        );

        const payload: TPayloadData<TFFAGameOverPayload> = {
            event: 'freeForAll/gameOver',
            data: { gameId, winnerId: user._id }
        };

        const gameSockets = utils.getAllSocketsFromGameRoom(gameId.toString());
        gameSockets?.forEach((socket) => {
            utils.sendDataToSocket(socket, payload);
        });
    } catch (error: any) {
        const { code, message, statusCode } = returnError(
            undefined,
            error.message ?? 'Something went wrong'
        );
        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }
}

export default onGameOver;
