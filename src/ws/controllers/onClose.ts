import { WebSocket } from 'ws';
import { IIncomingMessageWithUser } from '../../interfaces/ws';
import userModel from '../../models/user.model';
import { returnError } from '../../utilities/errors.util';
import utils, { userMap } from '../utils';

async function onClose(
    _socket: WebSocket,
    _request: IIncomingMessageWithUser,
    _code: number,
    _reason: Buffer
) {
    console.log(`log 1`);
    // handle codes: https://www.rfc-editor.org/rfc/rfc6455.html#section-7.1.5
    const user = _request.user;

    const userStatus = await userModel.updateUserStatus(user._id, 'offline');

    if (!userStatus) {
        const { code, message, statusCode } = returnError(
            'UPDATE_ERROR',
            'Could not update user status'
        );

        utils.sendErrorToSocket(_socket, { code, message, statusCode });
    }

    userMap.forEach((socket) => {
        if (socket !== _socket && socket.readyState === WebSocket.OPEN) {
            utils.sendDataToSocket(socket, {
                event: 'disconnected',
                data: {
                    username: user.username,
                    userStatus
                }
            });
        }
    });

    utils.deleteUserFromUserMap(user._id);
    console.log(`user deleted`);
}

export default onClose;
