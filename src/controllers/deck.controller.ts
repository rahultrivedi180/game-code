import { NextFunction, Request, Response } from 'express';
import { returnError } from '../utilities/errors.util';
import userModel from '../models/user.model';
import { is } from 'superstruct';
import deckValidator from '../validators/deck.validator';

export async function getDeck(
    _req: Request,
    _res: Response,
    _next: NextFunction
) {
    try {
        const query = _req.query;

        if (!is(query, deckValidator.GetDeck)) {
            return _next(
                returnError('INVALID_PAYLOAD', 'characterId is invalid')
            );
        }

        let decks = null;
        let deck = null;

        if (query.characterId) {
            const result = await userModel.default
                .findOne({
                    _id: query.userId,
                    'characters.characterId': query.characterId
                })
                .lean();
            deck = result?.characters.find(
                (value) => value.characterId === query.characterId
            );
        } else {
            const result = await userModel.default
                .findOne({
                    _id: query.userId
                })
                .lean();
            decks = result?.characters;
        }

        return _res.status(200).json(decks ?? deck);
    } catch (error: any) {
        _next(returnError(error.code, error.message));
        return;
    }
}

export async function updateDeck(
    _req: Request,
    _res: Response,
    _next: NextFunction
) {
    try {
        const [user, body] = [_req.user, _req.body];

        if (!is(body, deckValidator.UpdateDeck)) {
            return _next(
                returnError('INVALID_PAYLOAD', 'invalid request body')
            );
        }

        if (body.deck.length > 28) {
            return _next(
                returnError(
                    'INVALID_DECK_LENGTH',
                    'deck cannot have more than 28 cards'
                )
            );
        }

        const updateResult = await userModel.default
            .findOneAndUpdate(
                { _id: user._id, 'characters.characterId': body.characterId },
                { $set: { 'characters.$.deck': body.deck } },
                { new: true }
            )
            .lean();

        const deck = updateResult?.characters.find(
            (value) => value.characterId === body.characterId
        );

        return _res.status(200).json({ deck });
    } catch (error: any) {
        _next(returnError(error.code, error.message));
        return;
    }
}
