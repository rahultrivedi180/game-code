import friendModel from '../models/friend.model';
import friendValidators from '../validators/friend.validators';

import type { NextFunction, Request, Response } from 'express';
import { returnError } from '../utilities/errors.util';

export async function addFriend(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const body = req.body;
        const user = req.user;

        const validRequestBody =
            friendValidators.validateFriendRequestStatusUpdateBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        if (user._id !== body.requester) {
            return next(returnError('UNAUTHORIZED_REQUESTER'));
        }

        await friendModel.addFriend(body.requester, body.recipient);

        return res.status(201).json({ message: 'Friend Request Sent' });
    } catch (error: any) {
        next(returnError(error.code, error.message));
        return;
    }
}

export async function acceptFriendRequest(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const body = req.body;
        const user = req.user;

        const validRequestBody =
            friendValidators.validateFriendRequestStatusUpdateBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        if (user._id !== body.recipient) {
            return next(returnError('UNAUTHORIZED_RECIPIENT'));
        }

        const result = await friendModel.acceptFriendRequest(
            body.requester,
            body.recipient
        );

        return res.status(200).json({ result });
    } catch (error: any) {
        next(returnError(error.code, error.message));
        return;
    }
}

export async function rejectFriendRequest(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const body = req.body;
        const user = req.user;

        const validRequestBody =
            friendValidators.validateFriendRequestStatusUpdateBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        if (user._id !== body.recipient) {
            return next(returnError('UNAUTHORIZED_RECIPIENT'));
        }

        const result = await friendModel.rejectFriendRequest(
            body.requester,
            body.recipient
        );

        return res.status(200).json({ result });
    } catch (error: any) {
        next(returnError(error.code, error.message));
        return;
    }
}

export async function blockFriend(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const body = req.body;

        const validRequestBody =
            friendValidators.validateFriendRequestStatusUpdateBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        const result = await friendModel.blockFriend(
            body.requester,
            body.recipient
        );

        return res.status(200).json({ result });
    } catch (error) {
        next(error);
        return;
    }
}

export async function listFriends(
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const user = req.user;

        const [friends, pendingRequests, blockedUsers] = await Promise.all([
            friendModel.listFriends(user._id),
            friendModel.listPendingRequests(user._id),
            friendModel.listBlockedUsers(user._id)
        ]);

        return res.status(200).json({ friends, pendingRequests, blockedUsers });
    } catch (error) {
        next(error);
        return;
    }
}
