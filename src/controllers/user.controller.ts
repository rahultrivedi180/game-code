import { compare, hash } from 'bcrypt';
import sanitize from 'mongo-sanitize';

import userModel from '../models/user.model';
import jwtUtil from '../utilities/jwt.util';
import userValidator from '../validators/user.validators';

import type { NextFunction, Request, Response } from 'express';
import { returnError } from '../utilities/errors.util';

export async function signup(req: Request, res: Response, next: NextFunction) {
    try {
        const body = req.body;

        const validRequestBody = userValidator.validateSignupRequestBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        const { email, password, username } = body;
        const sanitizedEmail = sanitize(email);
        const sanitizedUsername = sanitize(username);

        const [userExists, duplicateUsername] = await Promise.all([
            userModel.getUserByParam('email', sanitizedEmail),
            userModel.getUserByParam('username', sanitizedUsername)
        ]);

        if (userExists) {
            return next(returnError('USER_EXISTS'));
        }

        if (duplicateUsername) {
            return next(returnError('DUPLICATE_USERNAME'));
        }

        const hashedPass = await hash(password, 10);

        const createdUser = await userModel.createUser({
            email: sanitizedEmail,
            password: hashedPass,
            username: sanitizedUsername,
            characters: []
        });

        const { _id, kmm } = createdUser;

        return res.status(201).json({ data: { _id, email, username, kmm } });
    } catch (error: any) {
        next(returnError(error.code, error.message));
        return;
    }
}

export async function signin(req: Request, res: Response, next: NextFunction) {
    try {
        const body = req.body;

        const validRequestBody = userValidator.validateSigninRequestBody(body);

        if (validRequestBody !== null) {
            return next(validRequestBody);
        }

        const { email, password } = body;
        const sanitizedEmail = sanitize(email);

        const user = await userModel.getUserByParam(
            'email',
            sanitizedEmail,
            true
        );

        if (!user) {
            return next(returnError('INCORRECT_CREDENTIALS'));
        }

        const { _id, username, kmm } = user;

        const passwordMatch = await compare(password, user.password);

        if (!passwordMatch) {
            return next(returnError('INCORRECT_CREDENTIALS'));
        }

        const signedToken = jwtUtil.sign({
            _id: _id.toString(),
            email: email,
            username: username
        });
        return res.status(200).json({
            data: {
                token: signedToken,
                user: { _id: _id.toString(), email, username, kmm }
            }
        });
    } catch (error) {
        next(error);
        return;
    }
}
