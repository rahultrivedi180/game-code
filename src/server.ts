import env, { initEnv } from './env';

// init env
console.log('[info] initializing env');
initEnv();

import http from 'http';
import mongoose from 'mongoose';
import app from './app';
import { websocketServer } from './ws/index';

const httpServer = http.createServer(app);

httpServer.listen({
    port: env().port,
    host: env().host
});

httpServer.on('upgrade', (request, socket, head) => {
    websocketServer.handleUpgrade(request, socket, head, (ws) => {
        websocketServer.emit('connection', ws, request);
    });
    websocketServer.on('error', console.error);
});

httpServer.on('listening', async () => {
    console.log(`[info] listening to ${env().host}:${env().port}`);
    mongoose.set('strictQuery', true);
    const db = await mongoose.connect(env().dbUri);
    console.log(`[info] connected to ${db.connection.db.databaseName}`);
});

process.on('unhandledRejection', (reason: Error) => {
    throw reason;
});

process.on('uncaughtException', (error: Error) => {
    console.error(error);
    process.exit(1);
});
