import { Router } from 'express';

import friendRoutes from './friend.routes';
import userRoutes from './user.routes';
import deckRoutes from './deck.routes';

const router = Router();

router.get('/ping', (_req, res) => {
    return res.status(200).json({ msg: 'OK' });
});

router.use('/user', userRoutes);
router.use('/friend', friendRoutes);
router.use('/deck', deckRoutes);

export default router;
