import { Router } from 'express';

import {
    acceptFriendRequest,
    addFriend,
    blockFriend,
    listFriends,
    rejectFriendRequest
} from '../controllers/friend.controller';
import authMiddleware from '../middlewares/auth.middleware';

const router = Router();

router.get('/list', authMiddleware, listFriends);

router
    .post('/add', authMiddleware, addFriend)
    .post('/block', authMiddleware, blockFriend);

router
    .patch('/accept', authMiddleware, acceptFriendRequest)
    .patch('/reject', authMiddleware, rejectFriendRequest);

export default router;
