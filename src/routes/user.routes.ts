import { Router } from 'express';

import { signin, signup } from '../controllers/user.controller';

const router = Router();

router.post('/signup', signup).post('/signin', signin);

export default router;
