import { Router } from 'express';

import { getDeck, updateDeck } from '../controllers/deck.controller';
import authMiddleware from '../middlewares/auth.middleware';

const router = Router();

router.get('/', authMiddleware, getDeck);
router.patch('/', authMiddleware, updateDeck);

export default router;
