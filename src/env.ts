import dotenv from 'dotenv';

import type { IEnv } from './interfaces/env.interfaces';

let _env: IEnv | undefined = undefined;

export function initEnv() {
    dotenv.config();

    _env = {
        dbUri: process.env['DB_URI'] || 'mongodb://127.0.0.1:27017/',
        debug: Boolean(process.env['DEBUG']) || true,
        host: process.env['HOST'] || 'localhost',
        port: process.env['PORT'] || 3002,
        trustProxy: Boolean(process.env['PROXY']) || false,
        jwtSecret: process.env['JWT_SECRET'] || 'secret'
    };

    if (_env.debug) {
        console.log('[info] DEBUG ENABLED');
    }

    if (_env.trustProxy) {
        console.log('[info] PROXY ENABLED');
    }

    return _env;
}

export default function env() {
    if (!_env) {
        throw new Error('[error] Environment Variables are not initialized!');
    }
    return _env;
}
